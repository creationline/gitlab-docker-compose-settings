export GITLAB_VERSION ?= 13.6.2

.PHONY: all
all: ;

.PHONY: up
up:
	@echo "GITLAB_VERSION: ${GITLAB_VERSION}"
	docker-compose up -d

.PHONY: run
run:
	@echo "GITLAB_VERSION: ${GITLAB_VERSION}"
	docker-compose up

.PHONY: down
down:
	docker-compose down

.PHONY: logs
logs:
	docker-compose logs -f

.PHONY: ps
ps:
	docker-compose ps

.PHONY: shell
shell:
	@echo "GITLAB_VERSION: ${GITLAB_VERSION}"
	docker-compose exec web bash

.PHONY: clean
clean:
	@echo "GITLAB_VERSION: ${GITLAB_VERSION}"
	sudo rm -rf volumes/${GITLAB_VERSION}

.PHONY: show-env
show-env:
	@echo "GITLAB_VERSION: ${GITLAB_VERSION}"